# Meltano Speedrun with Gitpod

This is [Meltano Speedrun](https://meltano.com/blog/speedrun-from-0-to-elt-in-90-seconds/) with [Gitpod](https://www.gitpod.io/). 💫

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/gtsiolis/meltano-speedrun-with-gitpod/)
